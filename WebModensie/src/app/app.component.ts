import {Component, HostListener, OnInit} from '@angular/core';
import {CookieService} from 'ngx-cookie-service';
import {UserAccount} from './core/data.model';
import {WebSocketService} from '../websocket/services/web-socket.service';
import {Router} from '@angular/router';
import {DataRequestService} from './core/data.service';
import {ChatMessageDto} from '../websocket/models/chatMessageDto';
import {delay} from 'rxjs/operators';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  title = 'Modensie Web Platform';
  cookieMessageVisibility = 'hidden';

  myUser: UserAccount;
  inputMessage: string;

  goToLink(url: string){
    window.open(url, '_self');
  }

  constructor(public webSocketService: WebSocketService, private router: Router, private dataRequestService: DataRequestService, private cookieService: CookieService) { }

  setCookieMessageVisibility(visibility: string): void {
    this.cookieMessageVisibility = visibility;
  }

  ngOnInit(): void {
    if (!this.cookieService.check('cookiesAgreed')) {
      this.setCookieMessageVisibility('visible');
    }

    // Chat
    if (!this.cookieService.check('userLogged')) {
      (document.querySelector('.chat-btn') as HTMLElement).style.display = 'none';
    }

    if (this.cookieService.check('userLogged')) {
      this.dataRequestService.getUserAccountDetails(this.cookieService.get('userLogged'))
        .subscribe(data => {
          this.myUser = data;

          this.webSocketService.openWebSocket();
        });
    }
  }

  agreeToCookies(): void {
    const cookiesAgreedExpire = new Date();
    cookiesAgreedExpire.setMinutes(cookiesAgreedExpire.getMinutes() + 525600); // 1 year
    this.cookieService.set('cookiesAgreed', '', cookiesAgreedExpire, '/');
    this.setCookieMessageVisibility('hidden');
  }

  sendMessage(inputMessage: string){
    if (inputMessage.length !== 0) {
      const chatMessageDto = new ChatMessageDto(this.myUser.username, this.myUser.name, inputMessage, new Date().toString().split(' ')[4]);
      this.webSocketService.sendMessage(chatMessageDto);
      this.inputMessage = '';
    }
  }
}
