import { Component, OnInit } from '@angular/core';
import {Router} from '@angular/router';
import {DataRequestService} from '../core/data.service';
import {CookieService} from 'ngx-cookie-service';
import {Assignment, AssignmentCourse, Course, ParticipantCourse} from '../core/data.model';

import {formatDate} from '@angular/common';
import {delay} from 'rxjs/operators';
import {timer} from 'rxjs';

@Component({
  selector: 'app-patient-page',
  templateUrl: './teacher-page.component.html',
  styleUrls: ['./teacher-page.component.css']
})
export class TeacherPageComponent implements OnInit {
  userCourses: string[];
  assignmentList: Array<Assignment>;

  courseNameSelected: string;
  newAssignment: Assignment = new Assignment();

  newCourse: Course  = new Course();
  participantCourse: ParticipantCourse = new ParticipantCourse();

  private assignmentCourse: AssignmentCourse = new AssignmentCourse();

  constructor(private router: Router, private dataRequestService: DataRequestService, public cookieService: CookieService) { }

  ngOnInit(): void {
    if (!this.cookieService.check('userLogged')) {
      this.router.navigate(['login_page']);
    } else
    if (this.cookieService.get('userRole') !== 'teacher' ) {
      this.router.navigate(['error_page']);
    }

    this.dataRequestService.getAllCoursesNameOfUser(this.cookieService.get('userLogged'))
      .subscribe(data => {
        this.userCourses = data;
      });
  }

  showAssignment(courseName: string): void {
    (document.querySelector('.table-teacher-assignment') as HTMLElement).style.display = 'block';
    this.dataRequestService.getAllAssignmentsOfCourse(courseName)
      .subscribe(data => {
        this.assignmentList = data;

        this.courseNameSelected = courseName;

        for (let i = 0; i < this.assignmentList.length; i++) {
          this.assignmentList[i].date = new Date(this.assignmentList[i].date).toDateString();
          switch (Number(this.assignmentList[i].type)){
            case 0:
              this.assignmentList[i].type = 'lesson';
              break;
            case 1:
              this.assignmentList[i].type = 'exercise';
              break;
            default:
              this.assignmentList[i].type = 'news';
              break;
          }
        }
      });
  }

  deleteCourse(courseName: string): void {
    this.dataRequestService.deleteCourse(courseName)
      .subscribe( data => location.reload());
  }

  deleteAssignment(assignmentName: string): void {
    this.dataRequestService.deleteAssignment(assignmentName, this.courseNameSelected)
      .subscribe( data => location.reload());
  }

  addAssignment(): void{
    if (this.newAssignment.name !== undefined) {
      this.newAssignment.date = formatDate(new Date(), 'yyyy-MM-dd', 'en');
      this.newAssignment.teacher = this.cookieService.get('userLogged');
      this.newAssignment.courseName = this.courseNameSelected;

      this.dataRequestService.addAssignment(this.newAssignment)
        .subscribe(data => location.reload());
    }
  }

  addCourse(): void{
    if (this.newCourse.name !== undefined) {
      this.dataRequestService.addCourse(this.newCourse)
        .subscribe( );
      this.participantCourse.courseName = this.newCourse.name;
      this.participantCourse.usernameUser = this.cookieService.get('userLogged');

      timer(1000).subscribe(x => {  this.dataRequestService.addParticipantToCourse(this.participantCourse)
        .subscribe(data2 => location.reload()); });
    }
  }

  editAssignment(assignment: Assignment): void {
    this.assignmentCourse.courseName = this.courseNameSelected;
    this.assignmentCourse.assignmentName = assignment.name;
    this.assignmentCourse.description = assignment.description;

    this.dataRequestService.editAssignment(this.assignmentCourse)
      .subscribe( data => location.reload());
  }

  logout(): void {
    this.cookieService.delete('userLogged');
    this.cookieService.delete('userRole');
  }
}
