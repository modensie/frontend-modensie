import { Component, OnInit } from '@angular/core';
import { DataRequestService} from '../core/data.service';
import {UserAccount, UserAccountPassword} from '../core/data.model';
import {Router} from '@angular/router';
import {CookieService} from 'ngx-cookie-service';
import {formatDate} from '@angular/common';
import {WebSocketService} from '../../websocket/services/web-socket.service';
import {ChatMessageDto} from '../../websocket/models/chatMessageDto';

@Component({
  selector: 'app-login-page',
  templateUrl: './login-page.component.html',
  styleUrls: ['./login-page.component.css']
})
export class LoginPageComponent implements OnInit {
  users: Array<UserAccount>;
  userAccount: UserAccountPassword = new UserAccountPassword();
  errorMessage: string;
  newUserAccount: UserAccount = new UserAccount();



  constructor(private router: Router, private dataRequestService: DataRequestService, private cookieService: CookieService, public webSocketService: WebSocketService) { }
  // Router is used for redirecting to other page .navigate
  // UserService is used to get functions from it .dataRequestService

  ngOnInit() { // Get data
    if (this.cookieService.check('userLogged')) {
      this.router.navigate(['main_page']);
    }

    this.dataRequestService.getUserAccounts().subscribe(data => {
      this.users = data;
    });
  }

  public login(): void {
    this.dataRequestService.getUserAccounts().subscribe(data => {
      this.users = data;
    });

    let validLogin = false;
    for (const userDetails of this.users) {
      if (this.userAccount.username === userDetails.username) {
        validLogin = true;
        if (this.userAccount.password === userDetails.password) {
          const loginExpire = new Date();
          loginExpire.setMinutes(loginExpire.getMinutes() + 10); // 10 minutes
          this.cookieService.set('userLogged', userDetails.username, loginExpire, '/'); // Store username in a cookie
          this.cookieService.set('userRole', userDetails.role, loginExpire, '/'); // Store username in a cookie

          // Chat
          this.webSocketService.openWebSocket();
          setTimeout(() => {
            this.dataRequestService.getUserAccountDetails(this.cookieService.get('userLogged'))
              .subscribe(data => {
                const chatMessageDto = new ChatMessageDto(data.username, data.name, data.name + ' (' + data.role + ') joined!', new Date().toString().split(' ')[4]);
                this.webSocketService.sendMessage(chatMessageDto);
              });
          }, 1000); // Wait for the websocket to load

          this.router.navigate(['main_page']);
        } else {
          this.errorMessage = 'Invalid password';
        }
      }
    }
    if (!validLogin) {
      this.errorMessage = 'Invalid credentials';
    }
  }

  // returns '' if nothing is wrong, an error message otherwise
  private passwordsSanityChecks(): string {
    let rv = '';
    if (this.newUserAccount.password !== this.newUserAccount.passwordCheck)
      rv = 'Password validation doesn\'t match the password';
    else if (this.newUserAccount.password.length < 8)
      rv = 'Password should be at least 8 characters long';
    else if (this.newUserAccount.password.match(/[a-z]/) == null)
      rv = 'Password must contain a lowercase character';
    else if (this.newUserAccount.password.match(/[A-Z]/) == null)
      rv = 'Password must contain an uppercase character';
    else if (this.newUserAccount.password.match(/[0-9]/) == null)
      rv = 'Password must contain at least one digit';
    return rv;
  }

  public register(): void {
    let psc = this.passwordsSanityChecks();
    if (this.newUserAccount.username !== undefined && this.newUserAccount.password !== undefined && this.newUserAccount.name !== undefined && this.newUserAccount.emailAddress !== undefined && psc == '')
    {
      const gender = (document.getElementById('SelectGender')) as HTMLSelectElement;
      const role = (document.getElementById('SelectRole')) as HTMLSelectElement;
      this.newUserAccount.gender = gender.options[gender.selectedIndex].value;
      this.newUserAccount.role = role.options[role.selectedIndex].value;

      this.newUserAccount.birthdate = formatDate(this.newUserAccount.birthdate, 'dd-MM-YYYY', 'en_US');

      this.dataRequestService.addUserAccount(this.newUserAccount)
        .subscribe(
          data => location.reload());
    }
    else {
      if (psc != '')
	this.errorMessage = psc;
      else
	this.errorMessage = 'Invalid credentials';
    }
  }

  public resetPassword() {
    this.dataRequestService.resetPassword(this.userAccount.username).
      subscribe(() => alert("Check your e-mail"));
  }

  public showField(fieldName: string): void {
    if (fieldName === 'loginField') {
      if ((document.querySelector('.login-form') as HTMLElement).style.display === 'inherit')
      {
        this.login();
      }
      else {
        this.errorMessage = '';
        (document.querySelector('.login-form') as HTMLElement).style.display = 'inherit';
        (document.querySelector('.card-register') as HTMLElement).style.display = 'none';
        (document.querySelector('.card-back') as HTMLElement).style.display = 'inherit';
      }
    }
    else if (fieldName === 'registerField') {
      if ((document.querySelector('.register-form') as HTMLElement).style.display === 'inherit')
      {
        this.register();
      }
      else {
        this.errorMessage = '';
        (document.querySelector('.register-form') as HTMLElement).style.display = 'inherit';
        (document.querySelector('.card-login') as HTMLElement).style.display = 'none';
        (document.querySelector('.card-back') as HTMLElement).style.display = 'inherit';
      }
    }
    else if (fieldName === 'backField') {
      (document.querySelector('.login-form') as HTMLElement).style.display = 'none';
      (document.querySelector('.register-form') as HTMLElement).style.display = 'none';
      (document.querySelector('.card-back') as HTMLElement).style.display = 'none';
      (document.querySelector('.card-register') as HTMLElement).style.display = 'inherit';
      (document.querySelector('.card-login') as HTMLElement).style.display = 'inherit';
    }
  }


}
