import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import {
  Assignment, AssignmentCourse, Course, Meeting, ParticipantCourse,
  UserAccount, UserAccountEmailAddress, UserAccountPassword
} from './data.model';
import {Observable} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class DataRequestService {

  hostURL = 'localhost:8080'; // Backend Service

  constructor(private http: HttpClient) {
  }

  // --------------- User Account
  // GET
  public getUserAccounts(): Observable<UserAccount[]> {
    return this.http.get<UserAccount[]>('//' + this.hostURL + '/userAccounts/allUsers');
  }

  public getUserAccountDetails(username: string): Observable<UserAccount> {
    return this.http.get<UserAccount>('//' + this.hostURL + '/userAccounts/AccountDetailsOfAnUsername?username=' + username);
  }

  public resetPassword(username: string): Observable<void> {
    return this.http.get<void>('//' + this.hostURL + '/userAccounts/resetPassword?username=' + username);
  }

  // INSERT
  public addUserAccount(user) {
    return this.http.post<UserAccount>('//' + this.hostURL + '/userAccounts/post/insertUser', user);
  }

  // PUT
  public editUserAccountEmailAddress(userEmailAddress) {
    return this.http.put<UserAccountEmailAddress>('//' + this.hostURL + '/userAccounts/put/editAccountEmailAddress', userEmailAddress);
  }

  public editUserAccountPassword(userPassword) {
    return this.http.put<UserAccountPassword>('//' + this.hostURL + '/userAccounts/put/editAccountPassword', userPassword);
  }

  // DELETE
  public deleteUserAccount(username) {
    return this.http.request('delete', '//' + this.hostURL + '/userAccounts/delete/deleteUser?username=' + username);
  }

  // --------------- Course
  // GET
  public getAllCourses(): Observable<Course[]> {
    return this.http.get<Course[]>('//' + this.hostURL + '/courses/allCourses');
  }

  public getAllCoursesName(): Observable<string[]> {
    return this.http.get<string[]>('//' + this.hostURL + '/courses/allCoursesName');
  }

  public getAllCoursesNameOfUser(username: string): Observable<string[]> {
    return this.http.get<string[]>('//' + this.hostURL + '/participantCourse/allCoursesOfUser?username=' + username);
  }

  public getAllCoursesNameNotFollowedByUser(username: string): Observable<string[]> {
    return this.http.get<string[]>('//' + this.hostURL + '/participantCourse/allCoursesNotFollowedByUser?username=' + username);
  }

  public getAllParticipantsOfCourse(course: string): Observable<UserAccount[]> {
    return this.http.get<UserAccount[]>('//' + this.hostURL + '/participantCourse/allParticipantsOfCourse?courseName=' + course);
  }

  public checkCourse(courseName: string): Observable<Course> {
    return this.http.get<Course>('//' + this.hostURL + '/courses/checkCourse?courseName=' + courseName);
  }

  public addCourse(course) {
    return this.http.post<Course>('//' + this.hostURL + '/courses/post/insertCourse', course);
  }

  // DELETE
  public deleteCourse(courseName) {
    return this.http.request('delete', '//' + this.hostURL + '/courses/delete/deleteCourse?courseName=' + courseName);
  }

  // --------------- Participant Course
  // INSERT
  public addParticipantToCourse(assignation) {
    return this.http.post<ParticipantCourse>('//' + this.hostURL + '/participantCourse/post/insertParticipantCourse', assignation);
  }

  // DELETE
  public deleteParticipantCourse(username: string, courseName: string) {
    return this.http.request('delete', '//' + this.hostURL + '/participantCourse/delete/deleteParticipantCourse?username=' + username + '&courseName=' + courseName);
  }

  // --------------- Assignment
  // GET
  public getAllAssignmentsOfCourse(courseName: string): Observable<Assignment[]> {
    return this.http.get<Assignment[]>('//' + this.hostURL + '/assignment/allAssignmentsOfCourse?courseName=' + courseName);
  }

  // INSERT
  public addAssignment(assignment) {
    return this.http.post<Assignment>('//' + this.hostURL + '/assignment/post/insertAssignment', assignment);
  }

  // PUT
  public editAssignment(assignmentCourse) {
    return this.http.put<AssignmentCourse>('//' + this.hostURL + '/assignment/put/editAssignment', assignmentCourse);
  }

  // DELETE
  public deleteAssignment(assignmentName: string, courseName: string) {
    return this.http.request('delete', '//' + this.hostURL + '/assignment/delete/deleteAssignmentCourse?assignmentName=' + assignmentName + '&courseName=' + courseName);
  }

  // --------------- Meeting
  // GET
  public getAllMeetingsOfUser(username: string): Observable<Meeting[]> {
    return this.http.get<Meeting[]>('//' + this.hostURL + '/meeting/allMeetingsOfUser?username=' + username);
  }
}
