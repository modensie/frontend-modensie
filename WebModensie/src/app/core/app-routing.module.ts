import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import {ErrorPageComponent} from '../error-page/error-page.component';
import {LoginPageComponent} from '../login-page/login-page.component';
import {MainPageComponent} from '../main-page/main-page.component';
import {StudentPageComponent} from '../student_page/student-page.component';
import {TeacherPageComponent} from '../teacher_page/teacher-page.component';

import {SharedService} from './shared-service';

const routes: Routes = [
  {path: 'login_page', component: LoginPageComponent}, // Add Routes (Redirection to page)
  {path: '', component: LoginPageComponent},

  {path: 'main_page', component: MainPageComponent},
  {path: 'student_page', component: StudentPageComponent},
  {path: 'teacher_page', component: TeacherPageComponent},

  {path: '404', component: ErrorPageComponent}, // Error Page
  {path: '**', redirectTo: '/404'}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
  declarations: [],
  providers: [SharedService] // Share variables between components
})
export class AppRoutingModule { }
