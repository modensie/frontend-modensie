// User Account
export class UserAccount {
  userAccountId: string;
  username: string;
  password: string;
  passwordCheck: string;
  name: string;
  birthdate: string;
  gender: string;
  emailAddress: string;
  role: string;
}

export class UserAccountEmailAddress {

  username: string;
  emailAddress: string;
}

export class UserAccountPassword {

  username: string;
  password: string;
}

// Course
export class Course {

  courseId: string;
  name: string;
  password: string;
}

// Participant Course
export class ParticipantCourse {
  courseName: string;
  usernameUser: string;
}

// Assignment
export class Assignment {

  courseName: string;
  name: string;
  teacher: string;
  date: string;
  type: string;
  description: string;
}

export class AssignmentCourse {

  assignmentName: string;
  courseName: string;
  description: string;
}

// Meeting
export class Meeting {

  courseName: string;
  name: string;
  room: string;
  start_time: string;
  end_time: string;
  description: string;
  link: string;
}
