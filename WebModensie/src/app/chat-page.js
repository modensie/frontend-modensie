var totalMessages = 0;
var newMessages = 0;

setInterval(checkMessages,1000);

// chat button toggle
$(document).on('click', '.chat-btn', function (e) {
  document.querySelector('.chat-popup').classList.toggle('show');

  if(getComputedStyle(document.querySelector('.chat-popup')).display === 'flex') // open
  {
    document.querySelector('#messageBody').scrollTop = document.querySelector('#messageBody').scrollHeight;
    document.querySelector('.badge-round-chat').style.display = 'none';
    totalMessages = $("#myList div").length;
    scroll();

    if (newMessages > 0) {
      document.querySelector('.badge-popup-chat').innerHTML = newMessages;
      document.querySelector('.badge-popup-chat').style.display = 'flex';
      setInterval(hideNotification,2000);
    }

  }
  else // close
  {
    totalMessages = $("#myList div").length;
  }
});

$(document).on('click', '.submit', function (e) {
  setTimeout(scroll,10);
});

// scroll bar
function scroll() {
  document.querySelector('#messageBody').scrollTop = document.querySelector('#messageBody').scrollHeight;
}
function hideNotification() {
  document.querySelector('.badge-popup-chat').style.display = 'none';
}

// Every 1 sec
function checkMessages() {
  newMessages = $("#myList div").length - totalMessages;
  document.querySelector('.badge-round-chat').innerHTML = newMessages;

  if(getComputedStyle(document.querySelector('.chat-popup')).display !== 'flex') // close
  {
    if (newMessages > 0) {
      $('.badge-round-chat').css('display','flex');
    } else {
      $('.badge-round-chat').css('display','none');
    }
  }
};

