import { Injectable } from '@angular/core';
import {ChatMessageDto} from '../models/chatMessageDto';
import {CookieService} from 'ngx-cookie-service';

@Injectable({
  providedIn: 'root'
})
export class WebSocketService {

  webSocket: WebSocket;
  chatMessages: ChatMessageDto[] = [];

  constructor(private cookieService: CookieService) { }

  public openWebSocket() {
    this.webSocket = new WebSocket('ws://localhost:8080/chat');

    this.webSocket.onopen = (event) => {
    };

    this.webSocket.onmessage = (event) => {
      const chatMessageDto = JSON.parse(event.data);
      if (this.cookieService.get('userLogged') !== chatMessageDto.user)
      {
        chatMessageDto.type = 'income';
        this.chatMessages.push(chatMessageDto);
      }
      else {
        chatMessageDto.type = 'out';
        this.chatMessages.push(chatMessageDto);
      }
    };

    this.webSocket.onclose = (event) => {
    };
  }

  public sendMessage(chatMessageDto: ChatMessageDto) {
    this.webSocket.send(JSON.stringify(chatMessageDto));
  }

  public closeWebSocket(user: string, fullName: string, role: string) {
    this.sendMessage(new ChatMessageDto(user, fullName, fullName + ' (' + role + ') left!',  new Date().toString().split(' ')[4]));
    this.webSocket.close();
  }
}
