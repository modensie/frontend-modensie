export class ChatMessageDto {
  user: string;
  fullName: string;
  message: string;
  time: string;
  type: string;

  constructor(user: string, fullName: string, message: string, time: string){
    this.user = user;
    this.fullName = fullName;
    this.message = message;
    this.time = time;
  }
}
