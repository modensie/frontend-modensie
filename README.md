# Frontend Modensie

Frontend Application for Modensie Platform (Angular)

**Server on localhost:4200**

## New update (17-Feb-2021)
1. The project needs two more libraries in order to work. Go to the _Frontend Modensie\WebModensie_ folder and run `npm install bootstrap` and `npm install jquery'.
2. Check the _Frontend Modensie\WebModensie\src\websocket\services\web-socket.service.ts_ file and change the following line **this.webSocket = new WebSocket('ws://localhost:8080/chat');** with your backend service if it's not running on _localhost_.

## Setup
1. Download and install [Node.js](https://nodejs.org/en/download/)
2. Clone the project using `git clone https://gitlab.com/modensie/frontend-modensie.git`
3. Install Angular using `sudo npm install -g @angular/cli`
4. Go to the _Frontend Modensie\WebModensie_ folder and run `npm install --save-dev @angular-devkit/build-angular`
5. If **ng2-google-charts** is missing when server starts, install the Google Charts library using the following command `npm i --save ng2-google-charts` in Frontend folder.
6. Check the _Frontend Modensie\WebModensie\src\app\core\data.service.ts_ file and change the following line **hostURL = 'localhost:8080';** with your backend service if it's not running on _localhost_.
7. After that you're ready to go, you can simply do `ng serve` to start the web server (or follow more detailed insructions [here](https://gitlab.com/modensie/frontend-modensie/-/tree/master/WebModensie)

## HTTPS*
To run Angular CLI over HTTPS, use the following command which generates a random certificate `ng server --ssl`. For a specific certificate use the following `ng server --ssl-cert ./[file_location]/certificate.crt --ssl-key ./[file_location]/privateKey.key`.

_*Note: Running Frontend App on HTTP and Backend on HTTPS, please change all the http requests to https in the Frontend Modensie\WebModensie\src\app\core\data.service.ts file.
